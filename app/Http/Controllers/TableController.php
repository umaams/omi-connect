<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;

class TableController extends Controller
{
    public function index(Request $request)
    {
        $tables = DB::connection('sqlsrv')->table("INFORMATION_SCHEMA.TABLES");
        return Datatables::of($tables)
        ->editColumn('TABLE_NAME', function ($table) {
            return "<a href='".route('home.detail', ['table' => $table->TABLE_NAME])."'>".$table->TABLE_NAME."</a>";
        })
        ->rawColumns(['TABLE_NAME'])
        ->make(true);
    }

    public function data(Request $request)
    {
        $data = DB::connection('sqlsrv')->table($request->get('table'));
        return Datatables::of($data)
        ->make(true);
    }
}
