@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">List Table</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="table" class="table table-hover table-striped">
                                    <thead>
                                        <tr class="info">
                                            <th>Table Name</th>
                                            <th>Table Type</th>
                                            <th>Table Catalog</th>
                                            <th>Table Schema</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('#table').DataTable({
    processing: true,
    serverSide: true,
    pageLength: 50,
    ajax: '{{ route('table.index') }}',
    columns: [
        { data: 'TABLE_NAME', name: 'TABLE_NAME' },
        { data: 'TABLE_TYPE', name: 'TABLE_TYPE' },
        { data: 'TABLE_CATALOG', name: 'TABLE_CATALOG' },
        { data: 'TABLE_SCHEMA', name: 'TABLE_SCHEMA' },
    ],
    language: { 
        "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
    }
});
</script>
@endsection