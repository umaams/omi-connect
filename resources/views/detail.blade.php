@extends('layouts.app')

@section('style')
<style>
div.dataTables_wrapper {
    width: 100%;
    margin: 0 auto;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Detail Table: <small>{{ $table }}</small></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-warning pull-right" href="{{ url()->previous() }}">Back to Table</a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <div role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data</a>
                                    </li>
                                    <li role="presentation">
                                    <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Columns</a>
                                    </li>
                                </ul>
                                
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-md-12">
                                                <table id="table" class="table table-hover table-striped">
                                                    <thead>
                                                        <tr class="info">
                                                            @foreach ($columns as $column)
                                                            <th style="vertical-align: middle;">{{ $column->COLUMN_NAME }}</th>    
                                                            @endforeach
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tab">
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-md-12">
                                                <table id="table2" class="table table-hover table-striped">
                                                    <thead>
                                                        <tr class="info">
                                                            <th>Column Name</th>
                                                            <th>Data Type</th>
                                                            <th>Is Nullable</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($columns as $column)
                                                        <tr>
                                                            <td>{{ $column->COLUMN_NAME }}</td>   
                                                            <td>{{ $column->DATA_TYPE }}</td>   
                                                            <td>{{ $column->IS_NULLABLE }}</td> 
                                                        </tr>  
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('#table').DataTable({
    processing: true,
    serverSide: true,
    scrollX: true,
    pageLength: 100,
    ajax: '{{ route('table.data') }}?table={{$table}}',
    columns: [
        @foreach($columns as $column)
        { data: '{{$column->COLUMN_NAME}}', name: '{{$column->COLUMN_NAME}}'@if ($column->DATA_TYPE == 'int' || $column->DATA_TYPE == 'decimal') , className: 'text-right' @endif },
        @endforeach
    ],
    language: { 
        "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
    }
});
$('#table2').DataTable();
</script>
@endsection